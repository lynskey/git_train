# git 入门培训

**本文档如果出现错位，请用vim打开**

**本文档放置在 https://gitee.com/lynskey/git_train.git**

**欢迎大家不断补充完善**


## git原理
本部分使用git底层命令叙述git的基本原理，希望通过采用from scratch的方式展现git内部模型，工作原理，此部分的命令属于底层命令，平时基本用不上，大家可以不用太关注，重点关注git的实现原理

当理解git原理后，再去学基本命令就非常容易理解和记忆了，因此我打破一般教程从简单开始的传统，而是直接开始从原理讲起

此部分重点参考《pro git》第十章、《git internals》等

### git 四个对象类型
git可以看做key/value数据库，采用sha1消息摘要作为key，文件内容作为value。通过四个对象以及引用建立起一套可追踪版本的管理系统

#### blob对象
```
$ cd /tmp/ && git init temp && cd temp
$ echo hello | git hash-object -w --stdin
  ce013625030ba8dba906f756967f9e9ca394464a
$ ls -l .git/objects/ce
  -r--r--r-- 1 dht dht 21 Aug  6 15:12 013625030ba8dba906f756967f9e9ca394464a
$ git cat-file -t ce013625  # 显示对象类型
  blob
$ git cat-file -s ce013625  # 显示blob长度
  6
$ git cat-file -p ce013625  # 显示blob内容
  hello
$ git cat-file -p ce013625 > hello.txt  # 恢复到文件系统中


$ echo 01 > 01.txt
$ git hash-object -w 01.txt
  8a0f05e166aa61225bf6649cb345f87416b5f509

$ find .git/objects -type f
  .git/objects/8a/0f05e166aa61225bf6649cb345f87416b5f509
  .git/objects/ce/013625030ba8dba906f756967f9e9ca394464a
```
文件做了修改，同名文件存储为不同的blob对象，git采用松散模式来存储blob对象。以后采用git gc或进行git push操作时才会进行压缩，并存储差异
```
$ echo 02 >> 01.txt
$ git hash-object -w 01.txt
  a0c0404ff2506151b99fd1726b90b87ad81f56e1
$ find .git/objects -type f
  .git/objects/a0/c0404ff2506151b99fd1726b90b87ad81f56e1
  .git/objects/8a/0f05e166aa61225bf6649cb345f87416b5f509
  .git/objects/ce/013625030ba8dba906f756967f9e9ca394464a

$ for i in `find .git/objects -type f | awk -F'/' '{print $3$4}'`; do echo -n $i" "; git cat-file -t $i; done
  a0c0404ff2506151b99fd1726b90b87ad81f56e1 blob
  8a0f05e166aa61225bf6649cb345f87416b5f509 blob
  ce013625030ba8dba906f756967f9e9ca394464a blob
```
####  tree 对象
tree对象存放文件名、权限、sha1之间的映射关系，也可以存储另外一个tree对象，通过tree对象将blob对象管理起来，构建目录关系

建立第一颗树
```
$ git update-index --add --cacheinfo 100644 ce013625030ba8dba906f756967f9e9ca394464a hello.txt
$ git status
$ git write-tree
  aaa96ced2d9a1c8e72c56b253a0e2fe78393feb7
$ git cat-file -p aaa96ced2d9a1c8e72c56b253a0e2fe78393feb7
  100644 blob ce013625030ba8dba906f756967f9e9ca394464a    hello.txt
```

建立第二颗树
```
$ git update-index --add --cacheinfo 100644 8a0f05e166aa61225bf6649cb345f87416b5f509 01.txt
$ git write-tree
  b9d059d0204bdf4e601eccb429402323abf3105e
$ git cat-file -p b9d059d0204bdf4e601eccb429402323abf3105e
  100644 blob 8a0f05e166aa61225bf6649cb345f87416b5f509    01.txt
  100644 blob ce013625030ba8dba906f756967f9e9ca394464a    hello.txt
```

建立第三颗树
```
$ git update-index --add --cacheinfo 100644 a0c0404ff2506151b99fd1726b90b87ad81f56e1 01.txt
# 建立第三颗树前看看暂存区里的内容
$ git ls-files -s
$ git write-tree
  185303d607d48996e32bfdfa8bb72ef4a4b6649b
$ git cat-file -p 185303d607d48996e32bfdfa8bb72ef4a4b6649b
  100644 blob a0c0404ff2506151b99fd1726b90b87ad81f56e1    01.txt
  100644 blob ce013625030ba8dba906f756967f9e9ca394464a    hello.txt
```

  观察整个objects对象库
  ```
    $ for i in `find .git/objects -type f | awk -F'/' '{print $3$4}'`; do echo -n $i" "; git cat-file -t $i; done
    b9d059d0204bdf4e601eccb429402323abf3105e tree
    a0c0404ff2506151b99fd1726b90b87ad81f56e1 blob
    aaa96ced2d9a1c8e72c56b253a0e2fe78393feb7 tree
    8a0f05e166aa61225bf6649cb345f87416b5f509 blob
    185303d607d48996e32bfdfa8bb72ef4a4b6649b tree
    ce013625030ba8dba906f756967f9e9ca394464a blob    
```

对象库关系图
```

        tree                    blob

            +--------+          +--------+--------+
        2   | b9d059 | ----*--> | 8a0f05 | 01.txt |
            +--------+     |    +--------+--------+
                           |
            +--------+     \    +--------+-----------+
        1   | aaa96c | *****->  | ce0136 | hello.txt |
            +--------+   /      +--------+-----------+
                         |
            +--------+   |      +--------+--------+
        3   | 185303 | --*--->  | a0c040 | 01.txt |
            +--------+          +--------+--------+
```

#### commit对象

当blob对象使用tree对象管理起来后，tree对象自身没有进行有效管理，这个时候需要采用commit对象进行进一步管理。commit对象里可以记录提交信息、提交人员信息、时间信息等等方便管理的内容
```
$ echo 'v1 add hello.txt' | git commit-tree aaa96ced2d9a1c8e72c56b253a0e2fe78393feb7
 7a279c67040e64f2653d44928cfd445e9219851b
$ echo 'v2 add 01.txt' | git commit-tree b9d059d0204bdf4e601eccb429402323abf3105e -p 7a279c67040e64f2653d44928cfd445e9219851b
 198b4621f260bbe17d86fa67ac12bb2f4be92555
$ echo 'v2 update 01.txt' | git commit-tree 185303d607d48996e32bfdfa8bb72ef4a4b6649b -p 198b4621f260bbe17d86fa67ac12bb2f4be92555
 2057831f0fee83b463ed6e3901f7c310d9dad2ad
```

通过上面3个命令，将3棵树组织串联起来，并赋予提交顺序关系，下面命令将看到历史信息`$ git log --stat 2057831f0fee83b463ed6e3901f7c310d9dad2ad`

再看看对象库
```
$ for i in `find .git/objects -type f | awk -F'/' '{print $3$4}'`; do echo -n $i" "; git cat-file -t $i; done
b9d059d0204bdf4e601eccb429402323abf3105e tree
2057831f0fee83b463ed6e3901f7c310d9dad2ad commit
7a279c67040e64f2653d44928cfd445e9219851b commit
a0c0404ff2506151b99fd1726b90b87ad81f56e1 blob
aaa96ced2d9a1c8e72c56b253a0e2fe78393feb7 tree
8a0f05e166aa61225bf6649cb345f87416b5f509 blob
185303d607d48996e32bfdfa8bb72ef4a4b6649b tree
ce013625030ba8dba906f756967f9e9ca394464a blob
198b4621f260bbe17d86fa67ac12bb2f4be92555 commit


commit           tree                    blob

+--------+       +--------+          +--------+--------+
| 198b46 | ----> | b9d059 | ----*--> | 8a0f05 | 01.txt |
+--------+       +--------+     |    +--------+--------+
                     |         |
+--------+       +--------+     \    +--------+-----------+
| 7a279c | ----> | aaa96c | *****->  | ce0136 | hello.txt |
+--------+       +--------+   /      +--------+-----------+
                   |         |
+--------+       +--------+   |      +--------+--------+
| 205783 | ----> | 185303 | --*--->  | a0c040 | 01.txt |
+--------+       +--------+          +--------+--------+
```

查看commit对象的内容
```
$ git cat-file -p 2057831f0fee83b463ed6e3901f7c310d9dad2ad
tree 185303d607d48996e32bfdfa8bb72ef4a4b6649b
parent 198b4621f260bbe17d86fa67ac12bb2f4be92555
author duht <duht@aliyun.com> 1565082296 +0800
committer duht <duht@aliyun.com> 1565082296 +0800

v2 update 01.txt
```

#### tag对象
commit对象的sha1不容易记忆，为了以后更好的使用一个具体的commit，需要建立tag。  tag分为轻量和附注tag，这里介绍的是附注tag(annotated tag)，轻量tag不是对象

```
$ git tag -a v1 -m "tag for v1" 7a279c67040e64f2653d44928cfd445e9219851b

$ for i in `find .git/objects -type f | awk -F'/' '{print $3$4}'`; do echo -n $i" "; git cat-file -t $i; done
 b9d059d0204bdf4e601eccb429402323abf3105e tree
 2057831f0fee83b463ed6e3901f7c310d9dad2ad commit
 7a279c67040e64f2653d44928cfd445e9219851b commit
 a0c0404ff2506151b99fd1726b90b87ad81f56e1 blob
 aaa96ced2d9a1c8e72c56b253a0e2fe78393feb7 tree
 8a0f05e166aa61225bf6649cb345f87416b5f509 blob
 185303d607d48996e32bfdfa8bb72ef4a4b6649b tree
 ce013625030ba8dba906f756967f9e9ca394464a blob
 443569e62fc4d381095ebedeadcc1a61962ba666 tag
 198b4621f260bbe17d86fa67ac12bb2f4be92555 commit

$ git cat-file -p 443569e62fc4d381095ebedeadcc1a61962ba666
 object 7a279c67040e64f2653d44928cfd445e9219851b
 type commit
 tag v1
 tagger duht <duht@aliyun.com> 1565084004 +0800

 tag for v1
```

 查看commit对象的内容
 ```
$ git cat-file -p 2057831f0fee83b463ed6e3901f7c310d9dad2ad
 tree 185303d607d48996e32bfdfa8bb72ef4a4b6649b
 parent 198b4621f260bbe17d86fa67ac12bb2f4be92555

  tag            commit           tree                    blob

                 +--------+       +--------+          +--------+--------+
                 | 198b46 | ----> | b9d059 | ----*--> | 8a0f05 | 01.txt |
                 +--------+       +--------+     |    +--------+--------+
                                       |         |
  +--------+     +--------+       +--------+     \    +--------+-----------+
  | 443569 | --> | 7a279c | ----> | aaa96c | *****->  | ce0136 | hello.txt |
  +--------+     +--------+       +--------+   /      +--------+-----------+
                                     |         |
                 +--------+       +--------+   |      +--------+--------+
                 | 205783 | ----> | 185303 | --*--->  | a0c040 | 01.txt |
                 +--------+       +--------+          +--------+--------+
```

#### dangling 对象
```
$ git fsck --lost-found
  Checking object directories: 100% (256/256), done.
  Checking objects: 100% (6952/6952), done.
  dangling blob 2b07b6236ed2e70ee8c3e92a26701d2084347a32
  dangling blob 190909469a3ee7314a83d01b3e8afd718ee91349
  dangling blob c14c4132f632dedf9501fc1ef2e619b8271f2159
  dangling blob 6ae309cf8331eb77caf0b11f70f15023b37d1259
  dangling blob 63a930f1fd5d0f4c66548a9feb1b53f2eddef054
  dangling blob 997042044ea7ffc4338281341565bd7468641989
  dangling blob 9636a7533183bac35167e4763940e85e0a5e8712

$ git gc
  Enumerating objects: 7254, done.
  Counting objects: 100% (7254/7254), done.
  Delta compression using up to 4 threads
  Compressing objects: 100% (2253/2253), done.
  Writing objects: 100% (7254/7254), done.
  Total 7254 (delta 5286), reused 6837 (delta 4955)

$ git fsck --lost-found
  Checking object directories: 100% (256/256), done.
  Checking objects: 100% (7254/7254), done.
  dangling blob 2b07b6236ed2e70ee8c3e92a26701d2084347a32
  dangling blob 190909469a3ee7314a83d01b3e8afd718ee91349
  dangling blob c14c4132f632dedf9501fc1ef2e619b8271f2159
  dangling blob 997042044ea7ffc4338281341565bd7468641989
```

清除
```
$ git prune -n
  dangling blob 2b07b6236ed2e70ee8c3e92a26701d2084347a32
  dangling blob 190909469a3ee7314a83d01b3e8afd718ee91349
  dangling blob c14c4132f632dedf9501fc1ef2e619b8271f2159
  dangling blob 997042044ea7ffc4338281341565bd7468641989
$ git prune
$ git fsck
```

### 引用
git是一个key/value数据库，通过上面四个对象组织起来，形成版本控制系统，但是commit对象的sha1值是不容易使用的，需要通过一个方便人员读的方式将commit对象组织起来，这里就需要使用引用

#### 分支  
分支仅仅是一个引用，本地分支存放在.git/refs/heads目录下，远程分支放在.git/refs/remotes/origin 目录下（默认只有一个远程库的情况）
分支同时也是一个文件，里面的内容是40个字节的commit的sha1值，加一个字节的换行

创建master分支，指向提交205783
```
$ git update-ref refs/heads/master 205783
$ cat .git/refs/heads/master
  2057831f0fee83b463ed6e3901f7c310d9dad2ad
$ git branch
  *master
$ git log --stat
```
创建另外一个dev分支，指向提交
```
$ git update-ref refs/heads/dev 7a279c
$ cat .git/refs/heads/dev
  7a279c67040e64f2653d44928cfd445e9219851b
$ git br
  dev
* master
$ git checkout dev
$ git log --stat
```
通过上面的例子可以看出分支的代价非常小，而且是O(1)的

#### HEAD引用
HEAD 文件是一个符号引用（symbolic reference），指向目前所在的分支。所谓符号引用，意味着它并不像普通引用那样包含一个 SHA-1 值——它是一个指向其他引用的指针。

```
$ cat .git/HEAD
  ref: refs/heads/dev

  branch         commit           tree                    blob

                 +--------+       +--------+          +--------+--------+
                 | 198b46 | ----> | b9d059 | ----*--> | 8a0f05 | 01.txt |
                 +--------+       +--------+     |    +--------+--------+
     HEAD                              |         |
  +--------+     +--------+       +--------+     \    +--------+-----------+
  |  dev   | --> | 7a279c | ----> | aaa96c | *****->  | ce0136 | hello.txt |
  +--------+     +--------+       +--------+   /      +--------+-----------+
                                     |         |
  +--------+     +--------+       +--------+   |      +--------+--------+
  | master | --> | 205783 | ----> | 185303 | --*--->  | a0c040 | 01.txt |
  +--------+     +--------+       +--------+          +--------+--------+
```

当我们执行 git commit时，该命令会创建一个提交对象，并用 HEAD 文件中那个引用所指向的 SHA-1 值设置其父提交字段。

下面为了简化，我们画图时将不再画tree和blob部分，在做一次提交前如下图：
```

      HEAD
        |
        V
       dev                          master
        |                             |
        V                             V
    +--------+     +--------+     +--------+
    | 7a279c | <-- | 198b46 | <-- | 205783 |
    +--------+     +--------+     +--------+
    
```

现在开始在dev分支上做一次提交
```
$ echo 02 > 02.txt
$ git add 02.txt
$ git ci -m "dev: add 02.txt"
$ git log
  commit c6e4ac82d140257c771d74c56152bf5019e612d3 (HEAD -> dev)
  Author: duht <duht@aliyun.com>
  Date:   Wed Aug 7 09:26:30 2019 +0800

      dev: add 02.txt

  commit 7a279c [注意：这里我把sha1值改成昨天的值，这样能保持文档的一致性]
  Author: duht <duht@aliyun.com>
  Date:   Wed Aug 7 09:19:37 2019 +0800

      v1 add hello.txt


                 HEAD
                   |
                   V
                  dev   
                   |
                   V
               +--------+       master
            /  | c6e4ac |         |
           /   +--------+         |
          V                       V
+--------+     +--------+     +--------+
| 7a279c | <-- | 198b46 | <-- | 205783 |
+--------+     +--------+     +--------+
```
HEAD应用随着分支切换而指向不同的分支，当有新提交时master引用是不断被更新的，而HEAD只有在切换分支时才改变

```
$ git check master

                  dev           HEAD   
                   |              |
                   V              V
               +--------+       master
            /  | c6e4ac |         |
           /   +--------+         |
          V                       V
+--------+     +--------+     +--------+
| 7a279c | <-- | 198b46 | <-- | 205783 |
+--------+     +--------+     +--------+
```
#### 标签引用
和分支比较类似，不介绍了，注意标签指向是固定的，而分支随着新提交而变动   
```
$ git update-ref refs/tags/v2 c6e4ac82d140257c771d74c56152bf5019e612d3
$ git tag
  v2
$ cat .git/refs/tags/v2
  c6e4ac82d140257c771d74c56152bf5019e612d3
```

#### 远程引用

当对一个远程库进行一次push操作后，Git 会记录下最近一次推送操作时每一个分支所对应的值,并保存在 refs/remotes 目录下

```
$ git clone git@gitee.com:lynskey/ex01.git
$ git remote -v
    origin    git@gitee.com:lynskey/ex01.git (fetch)
    origin    git@gitee.com:lynskey/ex01.git (push)
$ ls .git/refs/
    heads  tags
$ echo 1 > 1.txt
$ git add 1.txt
$ git ci -m "add 1.txt"
$ git push origin master   # 把本地master分支推送到远程库
$ ls .git/refs/
    heads  remotes  tags
$ cat .git/refs/remotes/origin/master
    25856f1d642dfd9f48e1f4c8be1c894a2645fb41
$ git log --oneline
    25856f1 (HEAD -> master, origin/master) add 1.txt
$ echo 2 >> 1.txt
$ git add 1.txt
$ git ci -m "master:2"
$ git log --oneline   # 可以看到远程分支已经落后本地分支一个提交
    7d9b0ad (HEAD -> master) master:2
    25856f1 (origin/master) add 1.txt
$ git push
$ git log --oneline  # 可以看到push后，远程分支引用移动到最新位置
    7d9b0ad (HEAD -> master, origin/master) master:2
    25856f1 add 1.txt

$ git clone git@gitee.com:lynskey/ex01.git
$ git remote -v
	origin	git@gitee.com:lynskey/ex01.git (fetch)
	origin	git@gitee.com:lynskey/ex01.git (push)
$ ls .git/refs/
	heads  tags
$ echo 1 > 1.txt
$ git add 1.txt
$ git ci -m "add 1.txt"
$ git push origin master   # 把本地master分支推送到远程库
$ ls .git/refs/
	heads  remotes  tags
$ cat .git/refs/remotes/origin/master
	25856f1d642dfd9f48e1f4c8be1c894a2645fb41
$ git log --oneline
	25856f1 (HEAD -> master, origin/master) add 1.txt
$ echo 2 >> 1.txt
$ git add 1.txt
$ git ci -m "master:2"
$ git log --oneline   # 可以看到远程分支已经落后本地分支一个提交
	7d9b0ad (HEAD -> master) master:2
	25856f1 (origin/master) add 1.txt
$ git push
$ git log --oneline  # 可以看到push后，远程分支引用移动到最新位置
	7d9b0ad (HEAD -> master, origin/master) master:2
	25856f1 add 1.txt
```

### git pack文件

从前面例子中可以看到即使同一个文件改动非常少，在存储时也是两个不同的对象，这样在磁盘空间上就比较浪费了。git的解决方法是在特定的时间点进行增量存储和压缩处理，比如两个文件非常相近，那么把最新的版本保留完本，老的保留差异。同时把文件以压缩格式写入一个pack文件
```
$ git gc
    对象计数中: 6, 完成.
    Delta compression using up to 4 threads.
    压缩对象中: 100% (2/2), 完成.
    写入对象中: 100% (6/6), 完成.
    Total 6 (delta 0), reused 0 (delta 0)

$ ls -l .git/objects/pack/
    总用量 8
    -r--r--r-- 1 dht dht 1240 8月  11 12:22 pack-058fb3694b137fdc8b3c50d6b810d255bd047e5e.idx
    -r--r--r-- 1 dht dht  404 8月  11 12:22 pack-058fb3694b137fdc8b3c50d6b810d255bd047e5e.pack
```

可以看到，一个pack文件一个是索引文件，有索引文件就可以快速定位
```
$ git verify-pack -v .git/objects/pack/pack-058fb3694b137fdc8b3c50d6b810d255bd047e5e
    7d9b0ad70af15999a3f30b65b346e56463ce9000 commit 197 144 12
    25856f1d642dfd9f48e1f4c8be1c894a2645fb41 commit 150 116 156
    1191247b6d9a206f6ba3d8ac79e26d041dd86941 blob   4 13 272
    5457b3462c78fd6d7590a15ad3be446a4fb5d1fd tree   33 44 285
    38fd29697b220f7e4ca15b044c3222eefe5afdc1 tree   33 44 329
    d00491fd7e5bb6fa28c517a0bb32b8b506539d4d blob   2 11 373
    非 delta：6 个对象
    .git/objects/pack/pack-058fb3694b137fdc8b3c50d6b810d255bd047e5e.pack: ok

```

## git基础

### 工作区、暂存区
#### 工作区
工作区就是当前分支的工作目录，包含此分支最新HEAD^{tree}中所有的文件、子目录等信息

#### 暂存区(index, stage)
开发中需要提交的修改内容，先放入此区域，目的是一个提交尽量包括一个整个功能，有时候修改一个文件时，可能要改若干个地方，有的已经改好，可以放到暂存区，还没修改好的可以不放入，这样就不会把不成熟的代码提交

`$ git add 1.txt`
1.txt加入暂存区，此时会在创建blob对象

`$ git add -u`
加入所有已经通过git进行管理的并且被改动过的文件

```
$ git status
On branch master
Changes to be committed:
  (use "git reset HEAD <file>..." to unstage)

    modified:   1.txt
```

`$ git reset HEAD 1.txt`
stage区删除1.txt，1.txt做的修改不会丢失

#### git“三棵树”
git中有三个重要的文件集合此处称为“三棵树”，注意区别于前面提到的tree概念。在执行git add/commit/checkout/reset时，主要是操作下面的三种文件集合（工作区、暂存区、HEAD），一般情况文件在三棵树中的变化如下：
```

+-------+          +---------+        +----------+
|working|          | staging |        |repository|
|       |          | (index) |        |  (HEAD)  |
+-------+          +---------+        +----------+
    ||                 ||                  ||
    ||                 ||                  ||
    ||     git add     ||                  ||
    |----------------->-|                  ||
    ||                 ||     git commit   ||
    ||                 |------------------>-|
    ||                 ||                  ||
    ||                 ||                  ||
    ||                 ||                  ||
    ||                 ||   git checkout   ||
    |-<-------------------------------------|
    ||                 ||   git reset      ||
    ||                 ||                  ||
    ++                 ++                  ++
```
其中working是工作区，即当前在文件系统中可以看到的文件集合。index是暂存区，即下次要commit的文件集合。HEAD指向当前分支，也是下次commit时的父节点。

例如查看当前暂存区文件：
```
$ git ls-files -s      
100644 259148fa18f9fb7ef58563f4ff15fc7b172339fb 0	.gitignore
100644 9c95bce87ba71d18e340cac606749bdbe18d0a18 0	A-successful-Git-branching-model.pdf
100644 d0cd403be2177d7ed9c95e0db92dc19dacf11637 0	README.md
100644 dfd7c0099e8c6344a5dae3fd938fb535b35f333b 0	Three-Way-Merging-A-Look-Under-the-Hood.pdf
100644 c578720451f94c8f9350e6d966e447b54fae926f 0	progit_v2.1.28.pdf
100644 000ec648b25abe4dc42cb0500a97e153029b7410 0	train.txt
```

查看当前HEAD的信息：
```
$ git cat-file -p .git/HEAD
tree 9ba4d916d5e3ff63411cfc708b41a2b6a081ba8e
parent 8e19ac9f37dd3f131bf542b6359a2b59b2bf54bd
author lwitcher <lwitcher@163.com> 1565789550 +0800
committer lwitcher <lwitcher@163.com> 1565789550 +0800

upd

$ git cat-file -t .git/HEAD
commit
```
HEAD指向的是最后一次commit。

若工作区中的文件被修改后，通过add将其加入暂存区，根据前面描述此时git为该文件创建的glob对象，此文件**当前内容**被暂存。以下是修改了train.txt后的操作：
```
$ git status       
位于分支 master
您的分支与上游分支 'origin/master' 一致。

尚未暂存以备提交的变更：
  （使用 "git add <文件>..." 更新要提交的内容）
  （使用 "git checkout -- <文件>..." 丢弃工作区的改动）

    修改：     train.txt
```
若已经加入暂存区的文件又被修改，git会识别文件被修改，被暂存的文件已经有了一个备份，此时工作区的文件被修改后依然可以用暂存区的备份将其恢复，此时还可以git add用工作区的最新版本替换暂存区的老版本文件：
```
$ git status
位于分支 master
您的分支与上游分支 'origin/master' 一致。

要提交的变更：
  （使用 "git reset HEAD <文件>..." 以取消暂存）

	修改：     train.txt

尚未暂存以备提交的变更：
  （使用 "git add <文件>..." 更新要提交的内容）
  （使用 "git checkout -- <文件>..." 丢弃工作区的改动）

	修改：     train.txt

```
git add后工作区的最新修改被存储到暂存区；git checkout train.txt后暂存区的文件被替换到工作区。


当相关文件都已暂存后，可以通过git commit为暂存区文件创建commit，此时分支指针指向最新创建的commit节点，HEAD指针也跟着移动，可以通过`git cat-file -p .git/HEAD`验证。

#### checkout&reset
checkout和reset两个命令都是对上述三棵树的操作，理解上面的概念对理解这两个命令的异同以及应用场景有很大帮助。最显著的差别之一是checkout会导致HEAD指向指定分支，从而切换下次提交的父节点；reset不会改变HEAD的指向，但会改变HEAD指向分支的指向，从而改变提交历史或下次提交的父节点。

**不带文件路径的checkout**
`git checkout [branch]`用来将工作区、暂存区、HEAD恢复成指定分支、tag或者提交。如果此时工作区中有文件被修改或暂存区有文件被修改，checkout会检查是否会导致修改丢失并给出错误提示(通过--force参数强制放弃修改)；若没有文件被修改则上述三棵树全部被指定分支替代。此外HEAD是一个指针，checkout更新HEAD的方式是简单的将HEAD指向branch。例如下面是执行`git checkout be1fe9`的结果：

```

                +-------+                
                | HEAD  |                
                +-------+                
                    |                    
                    v                    
                +-------+               +-------+       +-------+
                |  dev  |               | HEAD  |       |  dev  |
                +-------+               +-------+       +-------+
                    |                       |               |
                    v        ====>          v               v
+-------+       +-------+               +-------+       +-------+
|be1fe9 | <---- |394191 |               |be1fe9 | <---- |394191 |
+-------+       +-------+               +-------+       +-------+

```

通过上图可以知道，checkout不会对现存分支产生任何影响dev分支本身没有变化，仅会修改HEAD的指向。**注意此时HEAD并未指向任意一个分支（只指向commit），在该状态下提交的任何内容都会在checkout到其他分支上后被git遗弃。**若上述命令切换至一个具体分支，则HEAD指向指定分支，后续工作都将基于指定分支进行。

**带文件路径的checkout**
`git checkout [branch] [filename]`用指定分支中的指定文件替换暂存区和工作路径中的同名文件，与不带文件名的checkout不同的是不会移动HEAD指针。该命令可以用来将某分支上的指定文件用另一分支上的同名文件替换掉。

**不带文件路径的reset**
`git reset [option] [branch]`
reset不带文件路径时可以带如下参数：

--soft:将HEAD指针和HEAD指向的分支移动到指定分支上,不改变暂存区和工作区内容。

--mixed:reset不加参数时默认是--mixed行为。除了执行--soft的行为外，还用指定分支上的文件更新暂存区。

--hard:除了执行--mixed的行为外，还用指定分支上的文件更新工作区。

举例，在执行`git reset --soft be1fe9`后git树结构有像下面的变化：
```

                +-------+               +-------+
                | HEAD  |               |  HEAD |
                +-------+               +-------+
                    |                       |
                    v                       v
                +-------+               +-------+      
                |  dev  |               |  dev  |      
                +-------+               +-------+      
                    |                       |          
                    v        ====>          v          
+-------+       +-------+               +-------+       +-------+
|be1fe9 | <---- |394191 |               |be1fe9 | <---- |394191 |
+-------+       +-------+               +-------+       +-------+
```

执行之前如果已经有文件1.txt被修改并且add到暂存区，用git status查看可以发现该文件依然在暂存区等待commit，且工作区的文件在执行前后没有变化。
```
$ git status              
位于分支 master
要提交的变更：
  （使用 "git reset HEAD <文件>..." 以取消暂存）

    修改：     1.txt


git diff 1.txt
(比较工作区和暂存区指定文件，无不同)

git diff --cached 1.txt
(比较暂存区和HEAD指定文件，有改动)
```

`git reset --soft 394191`将状态恢复回reset之前的情况，使用`git reset --mixed be1fe9`，git的树结构和上图保持一致但暂存区已经修改的文件被394191提交的文件覆盖，此时使用git ststus查看：
```
$ git status               
位于分支 master
尚未暂存以备提交的变更：
  （使用 "git add <文件>..." 更新要提交的内容）
  （使用 "git checkout -- <文件>..." 丢弃工作区的改动）

    修改：     1.txt

修改尚未加入提交（使用 "git add" 和/或 "git commit -a"）
```
1.txt变成了未被缓存的状态，如果想提交的话需要先add。

`git reset --soft 394191 && git add 1.txt`将状态恢复回reset之前的清空，使用`git reset --hard be1fe9`，树结构同样和前两种情况一致，但此时工作区文件也被替换，之前对1.txt做的修改完全被抛弃：
```
$ git status              
位于分支 master
无文件要提交，干净的工作区
```

**带文件路径的reset**
通过上面的例子我们可以知道，reset主要操作对象是HEAD指针、暂存区和工作区。带路径的reset只支持不带--soft/--mixed/--hard选项，即只支持默认的--mixed行为，**尝试**更新暂存区和HEAD。

如果指定了文件路径，git是无法操作HEAD指针的————因为HAED指针指向的文件集合一部分文件属于老的branch，一部分属于新的branch，并不是一个有效提交，因此带路径的reset不会改变HEAD指针以及HEAD指针指向的分支。对于暂存区，git可以保证暂存区的内容被指定分支替换，因此带路径的reset最终的功能就是将指定分支中的指定文件替换至暂存区，不改变工作区和HEAD的任何内容。因此如果想放弃之前暂存的内容，例如不小心对不需要跟踪的文件使用了`git add 2.txt`，可以使用`git reset HEAD 2.txt`来放弃暂存，原理就是使用最后一次提交的2.txt替换暂存区的2.txt，即不再跟踪。

**checkout的使用场景**
不带路径的checkout可以修改HEAD指针、暂存区、工作区的文件。常规用法是切换当前所在分支，`git checkout dev`等，如果当前分支已经有修改的文件不想保存，加上`--force`参数即可。另一个常用场景是放弃当前所有的改动，恢复至当前分支最新代码：`git checkout --force HEAD`，注意此用法会将工作区和暂存区全部恢复，误操作可能导致修改的代码遗失。若开发过程中发现当前工作分支不对：例如应该在dev分支开发但是修改了master分支的代码，此时可以尝试用`git checkout dev`，将HEAD指针指向dev，但是假如git发现dev分支并不是master的父节点这个方法会报错，这种场景下需要在切换分支之前将修改过的文件`git stash`储藏，checkout到dev后再`git stash pop`后提交。

带路径的checkout不会修改HEAD指针，仅用指定版本的文件替换暂存区和工作区同名文件，在恢复版本、放弃修改时使用。

**reset的使用场景**
不带路径的reset可以包含三种参数，分别讨论：

首先看使用--soft参数的功能，作用是将HEAD指针和HEAD指针指向的分支移动，因此可以用来压缩提交历史。例如有如下提交历史：
```
                                +-------+                
                                | HEAD  |                
                                +-------+                
                                    |                    
                                    v                    
+-------+                       +-------+   
| master|                       |  dev  |   
+-------+                       +-------+   
    |                               |       
    v                               v     
+-------+       +-------+       +-------+   
|c6b89a | <---- |be1fe9 | <---- |394191 | 
+-------+       +-------+       +-------+ 
```
dev分支领先master分支2个提交，如果这两个分支中的代码由于修改错误/需求变化等原因导致需要放弃，而又不想在版本历史中体现出存在过这两个分支，可以使用`git reset --soft master`使dev分支重新指向master：

```
+-------+
| HEAD  |
+-------+
    |
    v
+-------+                                
|  dev  |                                
+-------+                              
    |                                                   
    v                                                   
+-------+                          
| master|                          
+-------+                          
    |                                  
    v                
+-------+       +-------+       +-------+   
|c6b89a | <---- |be1fe9 | <---- |394191 | 
+-------+       +-------+       +-------+ 
```
然而此时暂存区和工作区的修改都没有变化，文件内容依然包含2个提交的内容。此时使用`git commit`后结果如下：

```    
                +-------+               
                | HEAD  |               
                +-------+              
                    |                             
                    v                             
+-------+       +-------+                    
| master|       |  dev  |                    
+-------+       +-------+                   
    |               |                          
    v               v     
+-------+       +-------+    
|c6b89a | <---- |ec76rb |  
+-------+       +-------+ 
```
之前分支的多个提交被合并为一次提交。使用此方法可以精简提交历史,**需要注意的是如果dev分支已经push，可能被别人跟踪，那么一定不要用该方法（包括rebase），可能会导致其他人版本错乱，最好仅对本地分支执行此类操作。**

--mixed参数（或不加参数，默认为mixed）可以改变暂存区的文件，因此如果错误的对不该纳入版本控制中的文件用了git add，可以用reset命令将其移除暂存区，例如git add后使用git status命令，会有提示：
```
$git status
位于分支 dev
要提交的变更：
  （使用 "git reset HEAD <文件>..." 以取消暂存）
```

--hard参数还会将工作区的文件全部替换为指定分支，因此可以用来放弃本地修改。尽管git checkout也有相同作用，但与git checkout不同的是reset还可以同时放弃之前多次提交历史，如果经历了多次提交后想在该分支上废弃之前的提交，reset可以做到，而checkout仅能恢复工作区文件但无法做到删除历史提交记录。**需要注意的是如果dev分支已经push，可能被别人跟踪，那么一定不要用该方法（包括rebase），可能会导致其他人版本错乱，最好仅对本地分支执行此类操作。**checkout由于不修改提交历史，可以正常使用。git默认推荐的是使用checkout来放弃本地修改：
```
$git status
位于分支 dev
尚未暂存以备提交的变更：
  （使用 "git add <文件>..." 更新要提交的内容）
  （使用 "git checkout -- <文件>..." 丢弃工作区的改动）

```

带路径的reset仅有一个作用，即将暂存区的文件用指定分支文件替代。主要用在git add了错误的文件，例如某文件不需要跟踪却被add进了缓存区，在commit之前可以用`git reset HEAD FILENAME`取消暂存。或者希望某个文件恢复至某个历史版本，但不希望丢弃工作区的修改，也可以用该方式仅恢复暂存区并提交。

### git提交
暂存区里的变更确认需要提交时，可以采用git commit进行提交

#### 提交命令，方式和svn一样
```
$ git commit -m "test commit"
$ git commit -F commit_msg_file
```

#### 好的提交习惯
提交最好是一个完整的功能，相对比较独立，尽量不要提交零碎的功能，这样维护起来比较麻烦。提交信息要详细，开始是一条总体性的描述信息，接着下面是详细描述，因此建议采用`git commit -F commit_msg_file`方式进行提交

下面是一个好的提交
```
*************************************************************************
        commit d9ec37adc4bb493f63d0ab90a0c47df5f4c47075
        Author: Owen Anderson <owen.anderson@oculus.com>
        Date:   Fri Aug 2 12:29:18 2019 -0700

            Compress all non-Tensor components of a serialized TorchScript model. (#23723)

            Summary:
            This saves about 69KB off the FaceBlaze model, bringing the total size down from 388KB to 319KB.
             See https://github.com/pytorch/pytorch/issues/23582
            Pull Request resolved: https://github.com/pytorch/pytorch/pull/23723

            Differential Revision: D16623693

            fbshipit-source-id: 66267f87635c502c804293054fd5716d291389c0
*************************************************************************
```
### git日志
git log 命令特别多，可以通过 git help log进行学习
```
$ git log
$ git log --stat
$ git log -p
$ git log --graph
```

### 恢复被删除的文件
`$ git checkout -- deleted_file`

### git status查看当前分支状态
```
$ git status

位于分支 master
您的分支落后 'origin/master' 共 3 个提交，并且可以快进。
（使用 "git pull" 来更新您的本地分支）

尚未暂存以备提交的变更：
（使用 "git add/rm <文件>..." 更新要提交的内容）
（使用 "git checkout -- <文件>..." 丢弃工作区的改动）

修改：     caffe2/operators/quantized/int8_fc_op.cc
修改：     caffe2/quantization/server/fully_connected_dnnlowp_op.cc
修改：     docs/source/torch.rst
删除：     test/python
修改：     torch/_torch_docs.py
修改：     torch/csrc/jit/pickler.cpp


$ git status -s  
M caffe2/operators/quantized/int8_fc_op.cc
M caffe2/quantization/server/fully_connected_dnnlowp_op.cc
M docs/source/torch.rst
D test/python
M torch/_torch_docs.py
M torch/csrc/jit/pickler.cpp
```
### git 历史版本穿越
git 可以回退到任意一个版本
```
$ git log --oneline
7f130c84 (HEAD -> master, origin/master, origin/HEAD) Expose the quantized inputs and output of dynamic quantized int8 FC operator for debugging (#23566)
5faecc8b Perform string uniquing by value in pickle serialization. (#23741)
8e2b9de8 Document empty_strided (#23735)
f81db8af Initial torchbind prototype (#21098)
4e6e11c1 added opset10 ORT tests (#22993)

$ git reset f81db8af
```

重置后取消暂存的变更：
```
M    caffe2/operators/quantized/int8_fc_op.cc
M    caffe2/quantization/server/fully_connected_dnnlowp_op.cc
M    docs/source/torch.rst
D    test/python
M    torch/_torch_docs.py
M    torch/csrc/jit/pickler.cpp
$ git log --oneline
f81db8af (HEAD -> master) Initial torchbind prototype (#21098)
4e6e11c1 added opset10 ORT tests (#22993)
97917fd2 Partially revert "Remove all conda 3.5 nightly configs, remove libtorch smoketests (#21380)" (#23747)
```
如果想回到原来的提交，而又记不住提交的sha1值的话，需要使用 git reflog，具体参考git reflog介绍
```
$ git reflog
f81db8af (HEAD -> master) HEAD@{0}: reset: moving to f81db8af
7f130c84 (origin/master, origin/HEAD) HEAD@{1}: pull: Fast-forward
04f09d42 HEAD@{2}: pull: Fast-forward
5f24f9a2 HEAD@{3}: pull: Fast-forward
f6af76ea (grafted) HEAD@{4}: clone: from https://github.com/pytorch/pytorch/
```
恢复到原来的版本
```
$ git reset 7f130c84
7f130c84 (HEAD -> master, origin/master, origin/HEAD) Expose the quantized inputs and output of dynamic quantized int8 FC operator for debugging (#23566)
5faecc8b Perform string uniquing by value in pickle serialization. (#23741)
8e2b9de8 Document empty_strided (#23735)
f81db8af Initial torchbind prototype (#21098)
4e6e11c1 added opset10 ORT tests (#22993)
97917fd2 Partially revert "Remove all conda 3.5 nightly configs, remove libtorch smoketests (#21380)" (#23747)
    a1b10270 Fix the bug in regularizer matching (#23485)
```
上面通过git reset 方式更改版本的方式虽然有效，但是个人不是很喜欢使用这个命令，其实采用新建分支的方式，也可以达到相同的效果，具体参考git branch
`$ git checkout -b br_f81db8af f81db8af`

### git config
  不详细说明了
  
  `$ git config --list`  配置别名和svn命令一致，方便使用
  
  `$ git config alias.st status`
  
  `$ git config alias.co checkout`

### git 分支
git的杀手级命令，git最有价值的功能，从分支的实现原理得知，分支的开销极小，因此git鼓励大量、频繁的使用分支经常的使用场景，想实现一个比较复杂的功能，那么就可以基于当前分支创建一个分支，在上面进行开发，当新功能开发结束后合并到主干分支，接着删除新分支。可以频繁的创建、开发、合并、删除这些操作。

#### 建立分支
基于特定提交建立分支

`$ git br br_3107f1dc 3107f1dc`

基于tag建立分支

`$ git br v0.1-dev v0.1`　

#### 查看分支
分支前有'*'的是当前分支，工作区是当前分支的文件
```
$ git br
 *master
  dev

#显示更多分支信息
$ git br -vv

#显示远程分支信息
$ git br -r

#显示未被merge的分支
$ git br --no-merged
```
#### 删除分支
```
$ git br -d deleted_branch   # 如果此分支还没有被merge，删除会报错
$ git br -D deleted_branch　 # -D 选项强制删除分支，即使没有被merge　
```

#### 分支切换
```
$ git co v0.1-dev
$ git br
  *v0.1-dev
   master
```

#### 分支merge
**fast-forward合并（快进合并）**
为了说明分支合并，先建立下面测试环境
```
$ git init test && cd test
$ echo 01 > 01_master.txt
$ git add 01_master.txt
$ git ci -m "master: add 01_master.txt"
$ git co -b dev     # 创建一个新分支dev，并切换到新分支
$ echo dev_01 > 01_dev.txt
$ git add 01_dev.txt
$ git ci -m "dev: add 01_dev.txt"
$ git log
    commit 565b64c07e8f9dec17af39a8bb3a1c5ab73dbb66 (HEAD -> dev)
    Author: duht <duht@aliyun.com>
    Date:   Wed Aug 7 15:16:08 2019 +0800

        dev: add 01_dev.txt

    commit ef31572a2daf7dfa858bc5b29ee9c8c8fad23060 (master)
    Author: duht <duht@aliyun.com>
    Date:   Wed Aug 7 15:14:00 2019 +0800

        master: add 01_master.txt

                       +--------+       +-----+
                       | master |       | dev |
                       +--------+       +-----+
                           |               |
                           V               V   
                       +---------+     +--------+
                       | ef31572 | --> | 565b64 |
                       +---------+     +--------+


$ git co master     # 切换回master分支
$ git merge dev     # 将dev分支合并回master分支

  Updating 565b64..ef31572
  Fast-forward
   01_dev.txt | 1 +
   1 file changed, 1 insertion(+)
   create mode 100644 01_dev.txt


                                        +-----+
                                        | dev |
                                        +-----+
                                           |
                                           V   
                       +---------+     +--------+
                       | ef31572 | --> | 565b64 |
                       +---------+     +--------+
                                           ^
                                           |
                                       +--------+
                                       | master |
                                       +--------+


 ```
**非快进合并**
```
$ cd ../ && rm -rf test     # 清除前面的测试环境，重新建立
$ git init test && cd test
$ echo 01 > 01_master.txt
$ git add 01_master.txt
$ git ci -m "master: add 01_master.txt"
$ git co -b dev     # 创建一个新分支dev，并切换到新分支
$ echo dev_01 > 01_dev.txt
$ git add 01_dev.txt
$ git ci -m "dev: add 01_dev.txt"
$ git co master     # 切换回master分支
$ echo 02 >> 01_master.txt
$ git add -u
$ git ci -m "master: 02"
```
通过上面命令，我们创建了下面的图：
```

                           +........+      +-----+
                           . master .      | dev |
                           +........+      +-----+
                               |              |
                               V              V   
                           +--------+     +--------+
                           | 838925 | <-- | f47750 |
                           +--------+     +--------+
                               ^
                               |
                           +--------+     +--------+
                           | f48d1e | <-- | master |
                           +--------+     +--------+
```

可以从上图看到，f48d1e和f47750没有直接的父子关系，不能快进合并，对于这种场景有两种合并分支的方法，一种是即将介绍的三方向合并(three way merge)，以及下面要介绍的git rebase

三方向合并请参考，这里不讲了，在本git目录下有`Three-Way-Merging-A-Look-Under-the-Hood.pdf`

```
   +........+      +-----+                                   +-----+
   . master .      | dev |                                   | dev |
   +........+      +-----+                                   +-----+
       |              |                                         |
       V              V                                         V   
   +--------+     +--------+                 +--------+     +--------+
   | 838925 | <-- | f47750 |                 | 838925 | <-- | f47750 |
   +--------+     +--------+                 +--------+     +--------+
       ^                                         ^               ^
       |                       after merge       |               |
   +--------+     +--------+                 +--------+          |
   | f48d1e | <-- | master |                 | f48d1e |          |
   +--------+     +--------+                 +--------+          |
                                                 ^               |
                                                 |               |
                              +--------+     +--------+          |
                              | master | --> | f71a3c | ---------+
                              +--------+     +--------+     
                                              
#合并提交有两个父提交

$ git log --graph --oneline
*   f71a3c9 (HEAD -> master) Merge branch 'dev'
|\  
| * f477500 (dev) dev: add 01_dev.txt
* | f48d1ee master: 02
|/  
* 8389252 master: add 01_master.txt
```
显示合并提交的详细情况
```
$ git cat-file -p f71a3c9
  tree 4d19162b5c5f59121013e7e532434b16add83ab3
  parent f48d1ee26e437e437f3c95ca4bfca1df166559d9
  parent f4775007b84941484d4e756f2bc480e98eb3d3a8
  author duht <duht@aliyun.com> 1565166857 +0800
  committer duht <duht@aliyun.com> 1565166857 +0800

  Merge branch 'dev'
```

#### 分支rebase

**rebase解决合并历史复杂问题**

从上面三方向合并看，合并历史是比较复杂的，当项目分支合并比较频繁时提交历史会让人很头疼，大家欣赏一下下面这幅图

```
|\ \ \ \ \ \ \ \ \ \ \ \ \ \  
| * | | | | | | | | | | | | | daf79d6b73 Install 'future' when building Docker images
* | | | | | | | | | | | | | | ebf5869ed4 Added _VarHandlesOp and _ReadVariablesOp as recognized variables.
* | | | | | | | | | | | | | | 93386fe512 Allow use of Swift Cocoapod from TFLite examples
* | | | | | | | | | | | | | | d73703606b Don't pass --std=c++11 as a per-rule flag.
* | | | | | | | | | | | | | | 386e2de855 Allow weight_sparsity_map to accept both weight name substrings or regex
* | | | | | | | | | | | | | |   b6dedcb917 Merge pull request #25673 from Ryan-Qiyu-Jiang:env_capture_script_more_system_info_update
|\ \ \ \ \ \ \ \ \ \ \ \ \ \ \  
| * | | | | | | | | | | | | | | 8593e24a4f added python version, bazel version, and tf installed info to env_capture_script
* | | | | | | | | | | | | | | | aaa415996e Automated rollback of commit 92511b345c7fb72217cdb36bc0251596a9c68319
* | | | | | | | | | | | | | | | b71a4c0765 Switch RunHandler to use an inference friendly thread pool. Each inference has a dedicated work queue. All threads steal the work in the priority order of the request (currently arrival time). Note that there is one pool for both intra and inter work. However to avoid there are some thread the are not allowed to steal inter work, which can be blocking.
```
为了解决这个问题，我们需要采用rebase，rebase的基本思想：

首先找到这两个分支的最近共同祖先 838925，然后对比当前分支master相对于该祖先的历次提交，提取相应的修改并存为临时文件，然后将当前分支指向目标基底dev,最后以此将之前另存为临时文件的修改依序应用。

对上面的这段话，在当前场景下做一个大概的解释:
```
# git diff 838925 f48d1e > /tmp/diff_838925_f48d1e.txt
# 切换到dev分支
# patch /tmp/diff_838925_f48d1e.txt 到这个分支中
# 提交变更
# 切换回master分支
# 快进合并dev分支
```
操作例子：
```
$ cd ../ && rm -rf test     # 清除前面的测试环境，重新建立
$ git init test && cd test
$ echo 01 > 01_master.txt
$ git add 01_master.txt
$ git ci -m "master: add 01_master.txt"
$ git co -b dev     # 创建一个新分支dev，并切换到新分支
$ echo dev_01 > 01_dev.txt
$ git add 01_dev.txt
$ git ci -m "dev: add 01_dev.txt"
$ git co master     # 切换回master分支
$ echo 02 >> 01_master.txt
$ git add -u
$ git ci -m "master: 02"


                 +-----+  
                 | dev |  
                 +-----+  
                    |     
                    V     
+--------+     +--------+
| 721e05 | <-- | 4a25a4 |
+--------+     +--------+
    ^                    
    |                    
+--------+     +--------+
| 1f98f8 | <-- | master |
+--------+     +--------+
```

开始做rebase操作：
```
$ git co dev
$ git rebase master
 First, rewinding head to replay your work on top of it...
 Applying: dev: add 01_dev.txt
$ git log
commit 610022be49e40351d47489772aa2ec15638c050f (HEAD -> dev)
Author: duht <duht@aliyun.com>
Date:   Wed Aug 7 17:17:22 2019 +0800

    dev: add 01_dev.txt

commit 1f98f8a093a74ca3bf204899ee83e211c3d0498f (master)
Author: duht <duht@aliyun.com>
Date:   Wed Aug 7 17:17:22 2019 +0800

    master: 02

commit 721e05b37ea81d72321a4064c0ec6e99a673784b
Author: duht <duht@aliyun.com>
Date:   Wed Aug 7 17:17:22 2019 +0800

    master: add 01_master.txt


$ git co master
$ git merge dev
$ git log --graph
* commit 610022be49e40351d47489772aa2ec15638c050f (HEAD -> master, dev)
| Author: duht <duht@aliyun.com>
| Date:   Wed Aug 7 17:17:22 2019 +0800
|
|     dev: add 01_dev.txt
|
* commit 1f98f8a093a74ca3bf204899ee83e211c3d0498f
| Author: duht <duht@aliyun.com>
| Date:   Wed Aug 7 17:17:22 2019 +0800
|
|     master: 02
|
* commit 721e05b37ea81d72321a4064c0ec6e99a673784b
  Author: duht <duht@aliyun.com>
  Date:   Wed Aug 7 17:17:22 2019 +0800

      master: add 01_master.txt
```
可以看到我们通过git rebase获得了一个直线型的提交历史，而不是三方向合并导致的混乱提交历史。对于到底是采用merge还是rebase，分为两大流派，各自理由都非常有理，个人建议对于主干尽量采用rebase方式，对于开发分支可以采用三方向合并。

#### 利用git rebase压缩提交
有时候需要将几个提交合并为一个有意义的提交，可以采用rebase -i

```
$ cd ../ && rm -rf test     # 清除前面的测试环境，重新建立
$ git init test && cd test

# 开始第一个提交
$ echo 01 > 01_master.txt
$ git add 01_master.txt
$ git ci -m "master: add 01_master.txt 1"

# 开始第二个提交
$ echo 02 >> 01_master.txt
$ git add 01_master.txt
$ git ci -m "master: add 01_master.txt 2"

# 开始第三个提交
$ echo 03 >> 01_master.txt
$ git add 01_master.txt
$ git ci -m "master: add 01_master.txt 3"

# 查看一下当前的提交历史
$ git log --graph
* commit 89ddfe5d46e227e4b66a96ca911fd06f5addea8a (HEAD -> master)
| Author: duht <duht@aliyun.com>
| Date:   Tue Aug 20 09:43:27 2019 +0800
| 
|     master: add 01_master.txt 3
| 
* commit 1735359b6826d012576cb070167e3e693c74c36e
| Author: duht <duht@aliyun.com>
| Date:   Tue Aug 20 09:43:27 2019 +0800
| 
|     master: add 01_master.txt 2
| 
* commit d94a9f0e5e01fce13ccc363ca2ba4cc0240acc48
  Author: duht <duht@aliyun.com>
  Date:   Tue Aug 20 09:43:27 2019 +0800
  
      master: add 01_master.txt 1

# 开始采用rebase -i 将最新的两个提交压缩为一个提交
$ git rebase -i HEAD~2

pick 7136bb1 master: add 01_master.txt 2
pick d340ffa master: add 01_master.txt 3

改为：

r 7136bb1 master: add 01_master.txt 2
f d340ffa master: add 01_master.txt 3

存盘退出后，提示输入压缩后的提交信息，按照实际情况填写

$ git log --graph
* commit be286b68896bef8f6cb2ea15ebbb8383f7f1c0d3 (HEAD -> master)
| Author: duht <duht@aliyun.com>
| Date:   Tue Aug 20 10:44:15 2019 +0800
| 
|     squash two commit
| 
* commit be0694088aac50521872dba42de6c51ae36c61b8
  Author: duht <duht@aliyun.com>
  Date:   Tue Aug 20 10:44:15 2019 +0800
  
      master: add 01_master.txt 1

```

#### git merge --squash
利用git merge --squash压缩整个分支为一个提交，有时候我们开始一个特性分支开发
在开发过程中我们会进行比较零碎的提交，也可能在这个过程中再起N个分支进行子特性
开发，当一切全部开发测试结束后，在合并到主干前，我们不希望在主干中引入这么多
复杂的分支历史，这个时候可以利用git merge --squash来做到

```
这里举一个稍微复杂的例子

$ rm -rf test
$ git init test && cd test
$ echo 1 > 1.txt 
$ git add 1.txt
$ git ci -m "c1"

基于c1建立新分支dev
$ git co -b dev
$ echo 2 >> 1.txt
$ git ci -am "c2"

切换回master分支，建立c3
$ git co -
$ echo 3 >> 1.txt
$ git ci -am "c3"

切换回dev分支，建立c4、c5
$ git co - 
$ echo 4 >> 1.txt
$ git ci -am "c4"
$ echo 5 >> 1.txt
$ git ci -am "c5"

基于c5，建立新的特性开发分支dev2，并建立提交c6
$ git co -b dev2
$ echo 6 >> 1.txt
$ git ci -am "c6"

切换回dev分支，建立c7
$ git co -
$ echo 7 >> 1.txt
$ git ci -am "c7"

切换回dev2分支，建立c8
$ git co -
$ echo 8 >> 1.txt
$ git ci -am "c8"

切换回dev分支，合并dev2分支
$ git co dev
$ git merge dev2
解决合并冲突后，得到合并提交c9
删除dev2分支，已经合并完代码，此分支已经没有用处了
$ git br -d dev2

dev分支继续提交c10
$ echo 10 >> 1.txt
$ git ci -am "c10"

普通合并的提交历史
$ git log --graph --oneline
git log --graph --oneline
* 73e2ff4 (HEAD -> dev) c10
*   0faf5d3 merge dev2
|\  
| * aff9247 c8
| * b72b056 c6
* | dfc960d c7
|/  
* 63862b3 c5
* dba12dc c4
* 6c02abe c2
* 3863bfa c1

**至此特性分支dev全部开发测试完成，测试环境准备完毕**

合并前: 
                        +------+     +----+     +----+
                        | dev2 | ==> | c6 | <-- | c8 | <-------+
                        +------+     +----+     +----+         |
                                       |                       |
                                       V                       |
   +-----+     +----+     +----+     +----+     +----+       +----+     +-----+
   | dev | ==> | c2 | <-- | c4 | <-- | c5 | <-- | c7 | <---- | c9 | <-- | c10 |
   +-----+     +----+     +----+     +----+     +----+       +----+     +-----+
                 |
                 V
+--------+     +----+     +----+
| master | ==> | c1 | <-- | c3 |
+--------+     +----+     +----+

**此时拷贝一份test目录作为测试备份，开始进行两种合并方式的对比**
$ cd ../test
$ cp -a test test.bak && cd test

开始普通合并提交
$ git co master
$ git merge dev
解决可能出现的合并冲突，并提交得到合并提交c11
$ git log --oneline --graph
*   b966655 (HEAD -> master) merge dev
|\  
| * 73e2ff4 c10
| *   0faf5d3 merge dev2
| |\  
| | * aff9247 c8
| | * b72b056 c6
| * | dfc960d c7
| |/  
| * 63862b3 c5
| * dba12dc c4
| * 6c02abe c2
* | f62b9b4 c3
|/  
* 3863bfa c1

                        +------+     +----+     +----+
                        | dev2 | ==> | c6 | <-- | c8 | <-------+
                        +------+     +----+     +----+         |
                                       |                       |
                                       V                       |
   +-----+     +----+     +----+     +----+     +----+       +----+     +-----+
   | dev | ==> | c2 | <-- | c4 | <-- | c5 | <-- | c7 | <---- | c9 | <-- | c10 |
   +-----+     +----+     +----+     +----+     +----+       +----+     +-----+
                 |                                                         ^
                 V                                                         |
+--------+     +----+     +----+     +-----+                               |
| master | ==> | c1 | <-- | c3 | <-- | c11 | ------------------------------+
+--------+     +----+     +----+     +-----+
                                     合并提交   

可以看到提交历史非常复杂，但是准确的记录了提交历史

**现在开始恢复到备份的状态，即特性分支都开发好但还没有提交的状态**
$ cd .. && rm -rf test && cp -a test.bak test && cd test
$ git co master
**开始使用--squash来merge特性分支dev**
$ git merge --squash dev
解决合并冲突后，得到提交c11(注意c11不是合并提交，只有一个父提交)，并删除特性分支dev
$ git br -d dev

$ git log --graph --oneline
* ec930b0 (HEAD -> master) merge dev
* f62b9b4 c3
* 3863bfa c1

+--------+     +----+     +----+     +-----+
| master | ==> | c1 | <-- | c3 | <-- | c11 |
+--------+     +----+     +----+     +-----+

可以看到采用--squash选项后，整个特性分支的历史被压缩到一个合并中，这样在主干
上看，提交历史就非常简单清晰
```

#### git diff
查看各个版本、分支的差异，可以用git diff，diff格式和diff -urN是一样的

#### 合并、rebase冲突解决
合并冲突的形式和svn是一样的，解决冲突后的操作注意看命令提示信息，如果想放弃的话 `git merge --abort` 或 `git rebase --abort`

#### git archive
```
$ git archive --remote git@gitee.com:lynskey/git_train.git HEAD -o git_train.tar.gz --format tar.gz
$ git archive master --format tar.gz -o git_train_master.tar.gz
```

## 远程库
前面介绍基本都是基于本地git进行操作，git是一个分布式版本控制工具，linux kernel的开发者遍布全球，他们之间的开发协作全部使用git进行，因此远程库是非常重要的功能git强大的地方在于，git可以同时管理N个远程库

### 通过clone来建立本地库
```
$ git clone git@gitee.com:lynskey/ex02.git && cd ex02
$ git remote -v
    origin git@gitee.com:lynskey/ex02.git (fetch)
    origin git@gitee.com:lynskey/ex02.git (push)
$ git br
    * master
```

对于特别大的库，可以使用浅clone
```
$ git clone --depth=1 git@gitee.com:lynskey/ex02.git
```
对于浅clone的库，可以恢复深clone
```
$ git fetch --unshallow
```

对于特别大的库，可以只clone单个分支
```
git clone --branch master --single-branch git@gitee.com:lynskey/ex02.git
```


### 通过remote add添加多个远程库
```
$ rm -rf temp && git init temp && cd temp
$ git remote add git_train_ex01 git@gitee.com:lynskey/ex01.git
$ git remote add git_train_ex02 git@gitee.com:lynskey/ex02.git
$ git remote -v
    git_train_ex01 git@gitee.com:lynskey/ex01.git (fetch)
    git_train_ex01 git@gitee.com:lynskey/ex01.git (push)
    git_train_ex02 git@gitee.com:lynskey/ex02.git (fetch)
    git_train_ex02 git@gitee.com:lynskey/ex02.git (push)
$ git br
$ git br -r
$ git remote update git_train_ex01
    正在获取 git_train_ex01
    remote: Enumerating objects: 6, done.
    remote: Counting objects: 100% (6/6), done.
    remote: Compressing objects: 100% (2/2), done.
    remote: Total 6 (delta 0), reused 0 (delta 0)
    展开对象中: 100% (6/6), 完成.
    来自 gitee.com:lynskey/ex01
     * [新分支]          master     -> git_train_ex01/master

$ git remote update git_train_ex02
    正在获取 git_train_ex02
    warning: 没有共同的提交
    remote: Enumerating objects: 3, done.
    remote: Counting objects: 100% (3/3), done.
    remote: Compressing objects: 100% (2/2), done.
    remote: Total 3 (delta 0), reused 0 (delta 0)
    展开对象中: 100% (3/3), 完成.
    来自 gitee.com:lynskey/ex02
     * [新分支]          master     -> git_train_ex02/master

$ git br
$ git br -r
  git_train_ex01/master
  git_train_ex02/master
$ git merge git_train_ex01/master
$ echo 3 >> 1.txt && git add -u && git ci -m "master: 3"
$ git push -u git_train_ex01 master
  对象计数中: 3, 完成.
  写入对象中: 100% (3/3), 230 bytes | 230.00 KiB/s, 完成.
  Total 3 (delta 0), reused 0 (delta 0)
  remote: Powered By Gitee.com
  To gitee.com:lynskey/ex01.git
     7d9b0ad..97d4eaf  master -> master
  分支 'master' 设置为跟踪来自 'git_train_ex01' 的远程分支 'master'。
```

### git ls-remote
```
$ rm -rf temp && git init temp && cd temp
$ git remote add pytorch https://github.com/pytorch/pytorch.git
$ git ls-remote pytorch
  5ec1c293eb0c5745338e45d75f2f84f33dcb1137	HEAD
  3749c581b79cba49f511b19fa02c0f50fa05b250	refs/branches/4_new
  b4193ae0ea814ad117beb625f3deccbe4acc3129	refs/heads/21250-2
  642e14a2ecc20bfdd718fcc3a7c7274ec76e3961	refs/heads/AA
  92c3281c483d4b16faab582aae15ae6476b293b8	refs/heads/AddMoreBoolSupport
  ca2295cd9d1bb01b1d1114a088088546f0a13f88	refs/heads/FixOutMethodsForBool
  6728c76c99f7d037ca12d2896fec183a7c678e4e	refs/heads/SsnL-patch-1
```      


### git remote show
```
$ git remote show pytorch
* 远程 pytorch
获取地址：https://github.com/pytorch/pytorch.git
推送地址：https://github.com/pytorch/pytorch.git
HEAD 分支：master
远程分支：
21250-2                                              新的（下一次获取将存储于 remotes/pytorch）
AA                                                   新的（下一次获取将存储于 remotes/pytorch）
AddMoreBoolSupport                                   新的（下一次获取将存储于 remotes/pytorch）
FixOutMethodsForBool                                 新的（下一次获取将存储于 remotes/pytorch）
SsnL-patch-1                                         新的（下一次获取将存储于 remotes/pytorch）
SsnL-patch-2                                         新的（下一次获取将存储于 remotes/pytorch）
```

### git fetch/pull
git pull == git fetch + git merge
```
$ rm -rf temp && git init temp && cd temp
$ git remote add git_train_ex01 git@gitee.com:lynskey/ex01.git
$ git co master
$ git log --oneline
  97d4eaf (HEAD -> master, git_train_ex01/master) dev
  7d9b0ad master:2
  25856f1 add 1.txt

# 更换一个用户
$ cd /tmp/ && git clone git@gitee.com:lynskey/ex01.git
$ echo 2 > 2.txt
$ git add 2.txt
$ git ci -m "user2 add 2.txt"
$ git push

# 切换到原用户
$ echo 3 > 3.txt
$ git add 3.txt
$ git ci -m "user1 add 3.txt"
$ git push
  To gitee.com:lynskey/ex01.git
  ! [rejected]        master -> master (fetch first)
  error: 无法推送一些引用到 'git@gitee.com:lynskey/ex01.git'
  提示：更新被拒绝，因为远程仓库包含您本地尚不存在的提交。这通常是因为另外
  提示：一个仓库已向该引用进行了推送。再次推送前，您可能需要先整合远程变更
  提示：（如 'git pull ...'）。
  提示：详见 'git push --help' 中的 'Note about fast-forwards' 小节。

user1:

+----+       +----+
| c1 |  <--  | c3 |
+----+       +----+

user2 提交后remote:

+----+       +----+
| c1 |  <--  | c2 |
+----+       +----+

这个时候user1再push C3时，就会报上面的错误
```

### 采用git pull/merge方法解决push错误
```
$ git fetch git_train_ex01 master
  remote: Enumerating objects: 4, done.
  remote: Counting objects: 100% (4/4), done.
  remote: Compressing objects: 100% (2/2), done.
  remote: Total 3 (delta 0), reused 0 (delta 0)
  展开对象中: 100% (3/3), 完成.
  来自 gitee.com:lynskey/ex01
   * branch            master     -> FETCH_HEAD
     97d4eaf..e8fb905  master     -> git_train_ex01/master

$ cat .git/refs/remotes/git_train_ex01/master
  e8fb90541b3e26dd628a5e85ac75256ec5c70a3e
$ cat .git/refs/heads/master
  c0242fe923c30a093924f2095706906685f259e8
$ cat .git/FETCH_HEAD
  e8fb90541b3e26dd628a5e85ac75256ec5c70a3e		branch 'master' of gitee.com:lynskey/ex01

$ git merge git_train_ex01/master
  Merge made by the 'recursive' strategy.
   2.txt | 1 +
   1 file changed, 1 insertion(+)
   create mode 100644 2.txt
$ git log --oneline
  7f0e88d (HEAD -> master) Merge remote-tracking branch 'git_train_ex01/master'
  c0242fe add 3.txt
  e8fb905 (git_train_ex01/master) add 2.txt
  97d4eaf dev
  7d9b0ad master:2
  25856f1 add 1.txt
```

### 采用git pull方法解决git push错误
```
$ git pull
  Merge made by the 'recursive' strategy.
   2.txt | 1 +
   1 file changed, 1 insertion(+)
   create mode 100644 2.txt
$ git log --oneline
  154aeb6 (HEAD -> master) Merge branch 'master' of gitee.com:lynskey/ex01
  c0242fe add 3.txt
  e8fb905 (git_train_ex01/master) add 2.txt
  97d4eaf dev
  7d9b0ad master:2
  25856f1 add 1.txt

# 更新后进行push
$ git push
  对象计数中: 5, 完成.
  Delta compression using up to 4 threads.
  压缩对象中: 100% (4/4), 完成.
  写入对象中: 100% (5/5), 558 bytes | 558.00 KiB/s, 完成.
  Total 5 (delta 0), reused 0 (delta 0)
  remote: Powered By Gitee.com
  To gitee.com:lynskey/ex01.git
     e8fb905..d0389cb  master -> master
```

### 推送一个本地分支到远程库
```
$ git co -b dev
$ git push git_train_ex01 dev
  对象计数中: 3, 完成.
  Delta compression using up to 4 threads.
  压缩对象中: 100% (2/2), 完成.
  写入对象中: 100% (3/3), 254 bytes | 254.00 KiB/s, 完成.
  Total 3 (delta 1), reused 0 (delta 0)
  remote: Powered By Gitee.com
  To gitee.com:lynskey/ex01.git
   * [new branch]      dev -> dev
```

### 删除远程分支
```
$ git push git_train_ex01 :dev
  remote: Powered By Gitee.com
  To gitee.com:lynskey/ex01.git
   - [deleted]         dev
```

### 跟踪分支
当克隆一个仓库时，它通常会自动地创建一个跟踪 origin/master 的 master 分支。

用户2 pull，更新新的分支dev
```
$ git pull
  remote: Enumerating objects: 10, done.
  remote: Counting objects: 100% (10/10), done.
  remote: Compressing objects: 100% (6/6), done.
  remote: Total 8 (delta 1), reused 0 (delta 0)
  展开对象中: 100% (8/8), 完成.
  来自 gitee.com:lynskey/ex01
     e8fb905..d0389cb  master     -> origin/master
   * [新分支]          dev        -> origin/dev
  更新 e8fb905..d0389cb
  Fast-forward
   3.txt | 1 +
   1 file changed, 1 insertion(+)
   create mode 100644 3.txt

$ git co --track origin/dev
  分支 'dev' 设置为跟踪来自 'origin' 的远程分支 'dev'。
  切换到一个新分支 'dev'
```

===============================================================================

## git开发模型简介

**集中式模型**
一台服务器，N个开发者，这种模型和svn等相同

**司令官与副官工作流**


## git扩展
通过上面的学习，我们已经可以看到git的强大功能，如果仅仅把git当做一个版本管理工具的话，有点浪费了，我们可以在工作中扩展git的使用范围，让其更好的为我们服务

### 使用git管理测试环境
测试环境搭建是一个耗时费事的工作，有时候环境搭建要占整个测试工作的90%，如果我们能用git将一次搭建好的环境进行入库管理，每个测试场景采用不同的分支，边测试边维护测试版本，通过维护一份详细的README.txt，以后需要恢复测试场景时，基本上就是一下git操作可以大大节约时间（使用docker也可以达到同样目的，代价大一些）

### 设计方案中可以利用git原理
git分支、可回滚等特性非常强大，以后新项目中如果有需要类似场景时，可以考虑把git集成到设计方案中，丰富自己的设计能力。 https://github.com/libgit2/libgit2

### 生产环境管理
类似于1，生产环境的日志、配置、代码版本等等都可以用git管理起来，比如要进行全市场测试前，可以把生产环境都导入库中，然后部署全市场测试环境，全市场测试结束后，使用git命令可以恢复生产环境

## 状态迁移

### git文件状态概述
对于git库来说，任何文件存在如下几种状态：
- 未跟踪
- 未修改
- 已修改
- 已暂存

若某文件存在在git库路径但是未被纳入版本控制，则该文件状态为“未跟踪”。例如对于新文件test.txt：
```
$ echo "123" > test.txt && git status
位于分支 master
您的分支与上游分支 'origin/master' 一致。

未跟踪的文件:
  （使用 "git add <文件>..." 以包含要提交的内容）

    test.txt
```

此新文件未纳入版本控制，若使用git add，则文件被git跟踪，状态变为“已暂存”：

```
$ git add test.txt&& git status
位于分支 master
您的分支与上游分支 'origin/master' 一致。

要提交的变更：
  （使用 "git reset HEAD <文件>..." 以取消暂存）

    新文件：   test.txt
```
使用`git cat-file -p 190a18`可以看到git暂存了内容是123的blob。若此时使用git commit，则会为所有已暂存的文件创建commit，所有被commit的文件状态变为“未修改”。

```
$ git cat-file -p 8e19ac

tree a455c1346b97545867c76076dae30afb219fd227
parent 33b447b76856764fa1f24ba919c37e87188f34f1
author lwitcher <lwitcher@163.com> 1565789009 +0800
committer lwitcher <lwitcher@163.com> 1565789009 +0800

upd
```
可以看到git创建了tree,保存了之前的blob。此时再次修改工作区的文件，该文件状态又回到“已修改”，开始下一轮状态迁移。

在git路径下的所有文件都会轮流经历上述四个状态，如下所示：
```
+---------+       +----------+    +--------+    +--------+
|untracked|       |unmodified|    |modified|    | staged |
+---------+       +----------+    +--------+    +--------+
    ||                 ||            ||             ||
    ||                 ||            ||             ||
    ||    git add      ||            ||             ||
    |----------------->-|            ||             ||
    ||                 ||   edit     ||             ||
    ||                 |------------>-|             ||
    ||                 ||            ||  git add    ||
    || git rm --cached ||            |------------->-|
    |-<-----------------|            ||             ||
    ||                 ||            ||  git commit ||
    ||                 |-<---------------------------|
    ||                 ||            ||             ||
    ||                 ||            ||             ||
    ++                 ++            ++             ++
```

### 不再跟踪某文件
一个场景是错误的对不需要跟踪的文件执行了`git add`，再增加.gitignore文件是无法取消跟踪的，此时可以使用`git rm --cached FILENAME`不再跟踪指定文件。例如对于不需要跟踪的文件nouse:
```
$ git status
位于分支 dev
要提交的变更：
  （使用 "git reset HEAD <文件>..." 以取消暂存）

	新文件：   nouse

$ git commit -m upd
[dev 2040494] upd
 1 file changed, 0 insertions(+), 0 deletions(-)
 create mode 100644 nouse

$ git status
位于分支 dev
无文件要提交，干净的工作区
```
此时由于nouse文件已经提交，无法通过reset来取消暂存。使用`git rm --cached`后：
```
$ git rm --cached nouse 
rm 'nouse'

$ git status           
位于分支 dev
要提交的变更：
  （使用 "git reset HEAD <文件>..." 以取消暂存）

	删除：     nouse

未跟踪的文件:
  （使用 "git add <文件>..." 以包含要提交的内容）

	nouse
```
已经被删除的文件被识别到需要add来不再跟踪，需要commit来创建提交；同时由于工作区有一个暂存区没有的nouse，因此提示nouse未被跟踪。使用：
```
$ git commit -m del
[dev 69b4b6b] del
 1 file changed, 0 insertions(+), 0 deletions(-)
 delete mode 100644 nouse
 
 $ git status       
位于分支 dev
未跟踪的文件:
  （使用 "git add <文件>..." 以包含要提交的内容）

	nouse

提交为空，但是存在尚未跟踪的文件（使用 "git add" 建立跟踪）
```
确认文件已经不再跟踪。但是由于历史上的某个提交中还有该文件，其实并没有完全被删除，只是从此提交后git不再跟踪该文件了，**误提交了密码、个人信息等文件的话，该操作后依然是不安全的**。


### diff查看暂存区、工作区变化
git diff不加任何参数时对比的是工作区和暂存区的文件差异，当一个文件被添加到暂存区后使用git diff是没有任何输出的。如果想对比暂存区和HEAD的差别可以增加`--staged`参数。如果想对比工作区和HEAD的文件差异，可以使用`git diff FILENAME HEAD`来实现，也可以用该方法对比任何版本的文件差异；用`git diff --staged FILENAME HEAD`来实现暂存区文件和任意版本的文件对比。


