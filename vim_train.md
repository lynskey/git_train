# vim 常用命令、经验分享

**本文档如果出现错位，请用vim打开**

**本文档放置在 https://gitee.com/lynskey/git_train.git**

**欢迎大家不断补充完善**

本文档针对已经有经验的vim用户，起到一个温故知新的作用，因此特别基本的命令都会
一笔带过，同时附有点评，只有功能比较强大且需要经常使用的功能才会细讲，
对一些锦上添花的功能基本不涉及，同时只介绍vim自带功能，不涉及任何插件

vim没有任何技术难点，全部靠熟练程度，尽量做到不假思索的进行快捷键操作，这就要求
日常编辑工作尽量都使用vim来完成，达到手指记忆的程度。

如果是偶尔使用vim，需要熟悉基本命令的话: $vimtutor zh  

## 常用移动命令
```
1. h、j、k、l       左下上右 -- 尽量不要用方向键，手会离开键位，影响输入速度

         ^
         k          
   < h       l >   
         j        
         v

2. gg, G            文件第一行和最后一行
3. 50%              文件中间
4. ngg              定位到第n行，也可以使用ex模式下:n这个命令
5. ctrl+u(b)、ctrl+f(d)   向上、下翻页(半页)
6. H、M、L          光标定位到屏幕最上方、中间、最下面
7. zt,zz,zb         当前行移动到屏幕的最上方、中间、最下面
8. nj, nk           当前行下移、上移n行                
9. w, b (W、B)      向前、后移动一个word (注意 word 和 WORD 区别)
10. ^、0、$         移动到行首、行尾
11. %               
12. ctrl+e ctrl+y   光标文字同时向上、下移动一行
```

## 历史文件跳转
```
jumplist
1. ctrl+o  ctrl+i   向后、前跳转  
2. n+ctrl+o n+ctrl+i
3. :jump
4. :clearjumps

changelist  只能在本文件内跳转
4. g; g,
5. :changes

marks   
6. 'A  `A
7. :marks
```

## 范围表示
```
1. :%d           从头到尾 :% d
2. :ln,lm d      行n到m执行d命令
3. :.,.+100 d    从当前行到当前行+100执行d命令
4. :.,'a d       从当前行到mark 'a，执行d命令
5. :.,/regex/d   从当前行到匹配行，执行d命令 
6. :10,100w! >> a.txt 将10~100范围内的内容追加到文件a.txt中
```

## 剪切、拷贝黏贴
```
1. yy、nyy、Y
2. dd、ndd、D
3. p、P
4. ]p、[p          黏贴同时进行indent对齐
5. gp、gP          会把光标的位置移到被粘贴出来的文本结尾而不是开头
6. yw、ynw
7. dw、dnw
8. y%              拷贝括号(大中小)范围里的内容
9. yaw、daw       
```

## 寄存器
```
1. :reg            显示所有寄存器内容
2. ""              未命名(unnamed)寄存器，y、d、s、c、x等操作默认会存在这里一份
3. "0              复制专用寄存器，y操作默认会放这里一份，其他d、s、c、x等操作不会覆盖此寄存器
4. "1~9            删除行时的记录寄存器
5. "a-"z           有名寄存器，用小写字母引用有名寄存器，会覆盖该寄存器的原有内容，而换用大写字母的话，会将新内容添加到该寄存器的原有内容之后。
6. "ayy            
7. "cdd
8. "_              黑洞寄存器，只想删除文本却不想覆盖无名寄存器中的内容时，可以用此寄存器
9. "=              表达式寄存器，可以调用c库数学函数
10. "/             最后搜索模式寄存器
11. "%             当前文件名寄存器 
```
## mark
```
1. a-z             local mark
2. A-Z             global mark，可以跨文件跳转，也叫文件mark
3. :marks          显示所有的mark
4. :'a,'b y        拷贝mark a和 mark b之间的数据
5. :.,'a d         删除当前行和mark a之间的数据
6. 'a、`a          跳到mark a，注意区别，一个是跳到标记行，一个是跳到标记位置的字符
7. delmark a       删除mark

特殊mark
8. '0-'9           不能直接设置，‘0 代表退出vim时的位置，’1代表大上次退出vim时的文件位置
                   其他类似

实际测试和手册说的不一样，手册说： （太细节了，不用记）
9. '[              上次修改或yank位置的第一个字符
10. ']             上次修改或yank位置的最后一个字符
实际测试：
10. '[ `]          上次修改或yank位置的第一个字符
11. '] `[          上次修改或yank位置的最后一个字符

很少用，不用记
12. '> '<          上次选择visual block的开始、结束位置
```

## 搜索
```
1. :set ignorecase 设置忽略大小写
2. /\csearch_str   忽略大小写搜索 search_str
3. /\Csearch_str   区分大小写搜索 search_str
4. :set smartcase  如果模式全是由小写字母组成的，就会按照忽略大小写的方式查找，但只要输入一个大写字母，查找方式就会变成区分大小写的了
5. /\v\d+          采用very magic模式的正则表达式进行搜索
6. :set hls        高亮搜索
7. :set incsearch  增量搜索
8. *、#            
```

## 行内搜索
```
normal模式下的行内搜索是非常常用功能，一定有熟练掌握
1. f F ; ,          向后、前搜索一个字符，;继续搜下一个，','反向搜
2. t T ; ,          光标停在搜索字符前面
```

## 程序员常用指令
```
1. K               查找manpage
自动对齐
2. =G              在第一行执行后，可以把整个代码文件对齐
3. =%              在{}括号开始、或结束的地方执行，可以自动对齐{}范围内的代码

4. ctags           参考后面部分有详细说明
5. :syntax on      打开语法highlight
6. gd              找到临时变量的定义
7. :set sw=4       set shiftwidth=4
8. :set ts=4       set tabstop=4
9. :set expandtab  tab使用4字节代替，insert模式下，使用 ctrl+v+tab输入tab
10. :set noexpandtab
11. :cindent、smartindent、autoindent   建议咱们用cindent
```

### quickfix
这部分不常用，可以不关注
```
1. :make           不离开vim进行编译
2. :cw             显示quickfix窗口
3. :cn             定位到下一个bug
4. :cp             定位前一个bug
```

## insert模式编辑命令
```
这两个命令是bash命令
1. ctrl+w          向前删除一个词
2. ctrl+u          向前删除到行首
3. ctrl+d ctrl+t   insert模式下执行想左、右shift操作
```

## global命令
```
全局执行某个执行
1. :g /\v\d+/d         搜索全文将含有数字的行删除
2. :v /\v\d+/d         搜索全文将不含有数字的行删除
3. :g /\v\d+/ yank  A  将匹配的内容追加到指定的寄存器
```

## ctags
```
1. apt install exuberant-ctags
2. ctags -R .
3. ctrl+]         跳转到函数实现、变量定义的地方，结合 ctrl+o ctrl+i，可以实现快速切换
4. g+ctrl+]       如果发生了多处匹配的情况，g<C-]> 命令会从标签匹配列表中挑出可选项供我们选择
5. set tags=../tags 需要设置一下，否则会出现"No tags file"错误信息
```

## visual模式常用命令
```
  下面命令从normal模式转到visual模式
1. v             进入visual模式，按字节选
2. V             进入visual line模式，按行选
3. ctrl+v        进入visual block模式，按块选
4. D,Y,C         删除highlight行，即使没有选全
5. d,y,c         只删除选取部分
6. J, j          将选取部分合成一行
7. >, <          将选中的行向右、左移动shiftwidth字节
```
  
## shell命令输出到文件中
```
1. :r !ls -l
```

## 命令补全
```
1. ctrl+x+l           行补全
2. ctrl+n+p           
```

## 多文件编辑
## buffer
```
vim 1 2 3 ... vim可以同时打开n个文件进行编辑
vim在缓冲区进行编辑，打开一个文件时，在内存中存放此文件的映像，称为缓冲区

1. :ls (buffers)   查看打开的文件缓冲区，'%a'代表当前文件，'#'上次文件编辑的文件
                 ctrl+^ 可以在两个文件中进行切换（没什么用，不如用ctrl+o ctrl+i）   
2. :bp, bn, bl(ast), bf(irst) 缓冲区切换
3. :b3             调到特定标号3的buffer 
4. :bd 3 4 5       删除缓冲区3,4,5

ctrl+s ctrl+q  这个是shell的功能，如果误输入ctrl+s，屏幕锁定，ctrl+q恢复
```

## 参数列表
```
1. :args           查看参数列表
```

## windows
```
1. :vs、 ctrl+w+v     将当前窗口分为两个垂直窗口
2. :30vs abc.txt    分出30字节宽的窗口同时打开abc.txt
3. :5sp def.txt     水平分出5行窗口同时打开def.txt
4. ctrl+w+w         窗口来回切换
5. ctrl+w {hjkl}    窗口切换
```

## tab
```
1. :tabnew file1.txt 建立一个tab
2. gT、gt            向前、后遍历tab (:tabprev、tabnext等价)
3. Ngt               切换到第N个tab
4. :tabc             关闭一个tab
5. :tabo             保留当前tab，关闭其他
6. :tabmove n,       将当前tab移到第n个tab后面
7. :set mouse=a      使用鼠标来操作窗口、tab页选择等
8. :set mouse=i      关闭鼠标
```

## netrw  (如果想用这个功能，可以也尝试一下NerdTree插件)
```
1. :Lex 20     以20个字节宽带打开左侧边栏
2. t           以tab方式打开一个文件, gt/gT 进行tab切换
3. vi *.tar.gz 打开压缩文件进行浏览
```

## 折叠(fold)
```
   manual 折叠
1. :set foldmethod=manual 默认应该是manual
2. zf                     手动创建折叠 5zf，代表从当前行开始的5行折叠
3. zf5j  zf5k             向下折叠5行  向上折叠5行
4. zfa%                   在括号{处执行此命令，将整个{}里的东西进行折叠
5. zo zO                  打开折叠  递归打开折叠 
6. zc zC                  关闭折叠  递归关闭折叠 (从中间开始关闭外围)
7. zd zD                  删除折叠  递归删除折叠 (只能删除manual折叠)
8. zE                     删除所有折叠(只能删除manual折叠), E --> erase

   indent 折叠
9. :set foldmethod=indent 启动自动fold，zR打开所有folder
10. :set foldlevel=N       控制indent折叠显示的级别

   syntax 折叠
11. :set foldmethod=syntax 启动自动fold，zR打开所有folder

12. :mkview               保存折叠
13. :loadview             恢复折叠

14. zj zk                 在折叠之间移动
15. :set foldcolumn=N     在屏幕左侧显示一个折叠标识列，分别用“-”和“+”而表示打开和关闭的折叠。
```

## 宏
```
1. qa         normal模式下开始录制宏a
2. q          结束宏录制
3. N@a        应用宏N次
```

## swap文件
```
swap文件用来保存、跟踪对文件的修改，在宕机、ssh断开链接或vim异常退出后，可以用来恢复文件内容。
r: 恢复swap文件内容
d: 删除swap文件并放弃修改

:set noswapfile 不使用swap文件
```

## 加密文件
``` 
1 :X   可以对编辑的文件进行加密，vim加密后的文件，在打开时会提示输入密码
```

## 参考书籍
```
《Vim实用技巧（第2版）》
《Learing vi & vim》
《pro vim》
 http://www.truth.sk/vim/vimbook-OPL.pdf
```
